/**
 * Created by Алина on 15.01.2022.
 */

// Теоретические вопросы
    // 1. Описать своими словами для чего вообще нужны функции в программировании.
    // Функции позволяют использовать некий блок кода (который прописан в функции) повторно и
    // в любом месте. Это помогает структурировать и сокращать код.
    // 2. Описать своими словами, зачем в функцию передавать аргумент.
    // Аргументы присваивают параметрам функции определенные значения, которыми функция может оперировать.


let firstNumber = +prompt("Enter the first number");
let secondNumber = +prompt("Enter the second number");

while (!firstNumber && !secondNumber) {
    firstNumber = +prompt("Incorrect value! Enter the first number again");
    secondNumber = +prompt("Enter the second number again");
}
let operationSign = prompt("What operation to perform on numbers (+, -, *, /)");
while (operationSign !== "+" && operationSign !== "-" && operationSign !== "*" && operationSign !== "/") {
    operationSign = prompt("Sorry, math function not recognized. Enter '+', or '-', or '*', or '/'");
}

function getCalculations(a, b) {
    if (operationSign === "+") {
        return a + b;
    } else if (operationSign === "-") {
        return a - b;
    } else if (operationSign === "*") {
        return a * b;
    } else {
        if (b !== 0) {
            return a / b
        }
        return alert("Divide by zero error encountered!");
    }
}

console.log(getCalculations(firstNumber, secondNumber))